const std = @import("std");
const c = @import("c.zig");
const display = @import("display.zig");
const vec = @import("vec.zig");
const triangle = @import("triangle.zig");
const mesh = @import("mesh.zig");
const load_obj_file = @import("obj.zig").load_obj_file;
const Color = @import("color.zig").Color;
const sort = @import("sort.zig");
const Mat4 = @import("matrix.zig").Mat4;

const camera_position: vec.Vec3 = vec.Vec3.init(0, 0, 0);
var perspective_matrix: Mat4 = undefined;

var is_running: bool = true;
var previous_frame_time: isize = 0;

var triangles_to_render: std.ArrayListAligned(triangle.Triangle, null) = std.ArrayList(triangle.Triangle).init(std.heap.c_allocator);

const FILL_COLOR = Color.init(255, 255, 255, 255);
const WIRE_FRAME_COLOR = Color.init(255, 0, 255, 0);
const VERTEX_POINT_COLOR = Color.init(255, 0, 12, 240);

var render_mode = display.RenderMode.FILLED;
var culling_enabled = false;

pub fn setup() bool {
    const size = display.WIDTH * display.HEIGHT;
    if (std.heap.c_allocator.alloc(u32, size)) |payload| {
        display.color_buffer = payload;
    } else |_| {
        std.debug.print("ERROR: Failed to allocate memory for the color buffer\n", .{});
        return false;
    }

    perspective_matrix = Mat4.create_perspective_matrix(std.math.pi / 35.0, @as(f32, @floatFromInt(display.HEIGHT)) / @as(f32, @floatFromInt(display.WIDTH)), 0.1, 100.0);
    display.color_buffer_texture = c.SDL_CreateTexture(display.renderer, c.SDL_PIXELFORMAT_ARGB8888, c.SDL_TEXTUREACCESS_STREAMING, @as(c_int, @intCast(display.WIDTH)), @as(c_int, @intCast(display.HEIGHT)));

    if (display.color_buffer_texture == null) {
        std.debug.print("Failed to create color buffer texture\n", .{});
        return false;
    }

    // if (!load_obj_file("mesh/cube.obj", &mesh.mesh)) {
    //     std.debug.print("Failed to load mesh file\n", .{});
    //     return false;
    // }
    mesh.load_cube_mesh_data();
    return true;
}

pub fn process_input() void {
    var event: c.SDL_Event = undefined;
    _ = c.SDL_PollEvent(&event);
    switch (event.type) {
        c.SDL_QUIT => {
            std.debug.print("Window quit event\n", .{});
            is_running = false;
        },
        c.SDL_KEYDOWN => {
            if (event.key.keysym.sym == c.SDLK_ESCAPE) {
                std.debug.print("Escape key pressed\n", .{});
                is_running = false;
            } else if (event.key.keysym.sym == c.SDLK_1) {
                std.debug.print("render mode changed to WIRE_AND_VERTEX\n", .{});
                render_mode = display.RenderMode.WIRE_AND_VERTEX;
            } else if (event.key.keysym.sym == c.SDLK_2) {
                std.debug.print("render mode changed to WIRE\n", .{});
                render_mode = display.RenderMode.WIRE;
            } else if (event.key.keysym.sym == c.SDLK_3) {
                std.debug.print("render mode changed to FILLED\n", .{});
                render_mode = display.RenderMode.FILLED;
            } else if (event.key.keysym.sym == c.SDLK_4) {
                std.debug.print("render mode changed to FILLED_AND_WIRE\n", .{});
                render_mode = display.RenderMode.FILLED_AND_WIRE;
            } else if (event.key.keysym.sym == c.SDLK_5) {
                std.debug.print("culling enabled\n", .{});
                culling_enabled = true;
            } else if (event.key.keysym.sym == c.SDLK_6) {
                std.debug.print("culling disabled\n", .{});
                culling_enabled = false;
            } else {}
        },
        else => {},
    }
}

pub fn update() void {
    const time_to_wait: isize = display.FRAME_TARGET_TIME - (@as(isize, c.SDL_GetTicks()) - previous_frame_time);
    if (time_to_wait > 0 and time_to_wait <= display.FRAME_TARGET_TIME) {
        c.SDL_Delay(@as(u32, @intCast(time_to_wait)));
    }

    previous_frame_time = @intCast(c.SDL_GetTicks());

    mesh.mesh.rotation.x += 0.01;
    mesh.mesh.rotation.y += 0.01;
    mesh.mesh.rotation.z += 0.01;

    mesh.mesh.scale.y += 0.001;

    mesh.mesh.translation.z = 90.0;
    mesh.mesh.translation.x += 0.001;

    // // Create scale matrix to multiply the mesh vertices.

    var world_matrix: Mat4 = Mat4.create_identity_matrix();
    const scale_matrix: Mat4 = Mat4.create_scale_matrix(mesh.mesh.scale.x, mesh.mesh.scale.y, mesh.mesh.scale.z);
    const translation_matrix: Mat4 = Mat4.create_translation_matrix(mesh.mesh.translation.x, mesh.mesh.translation.y, mesh.mesh.translation.z);
    const rotation_matrix_x: Mat4 = Mat4.create_rotation_x(mesh.mesh.rotation.x);
    const rotation_matrix_y: Mat4 = Mat4.create_rotation_y(mesh.mesh.rotation.y);
    const rotation_matrix_z: Mat4 = Mat4.create_rotation_z(mesh.mesh.rotation.z);

    world_matrix = Mat4.multiply_matrix4(&scale_matrix, &world_matrix);
    world_matrix = Mat4.multiply_matrix4(&rotation_matrix_x, &world_matrix);
    world_matrix = Mat4.multiply_matrix4(&rotation_matrix_y, &world_matrix);
    world_matrix = Mat4.multiply_matrix4(&rotation_matrix_z, &world_matrix);
    world_matrix = Mat4.multiply_matrix4(&translation_matrix, &world_matrix);

    for (0..mesh.mesh.faces.items.len) |i| {
        const mesh_face: triangle.Face = mesh.mesh.faces.items[i];
        const face_vertices: [3]vec.Vec3 = .{
            mesh.mesh.vertices.items[@as(usize, @intCast(mesh_face.a - 1))],
            mesh.mesh.vertices.items[@as(usize, @intCast(mesh_face.b - 1))],
            mesh.mesh.vertices.items[@as(usize, @intCast(mesh_face.c - 1))],
        };

        var transformed_vertices: [3]vec.Vec4 = undefined;

        // Loop all three vertices of the current face and apply transformations
        for (0..3) |j| {
            var transformed_vertex: vec.Vec4 = face_vertices[j].to_vec4();

            // Perform translation, scaling and scaling of the cube.
            transformed_vertex = world_matrix.multiply_vector4(&transformed_vertex);

            transformed_vertices[j] = transformed_vertex;
        }

        // Culling. Skip this if culling has been disabled
        if (culling_enabled) {
            const vert_a: vec.Vec3 = transformed_vertices[0].to_vec3();
            const vert_b: vec.Vec3 = transformed_vertices[1].to_vec3();
            const vert_c: vec.Vec3 = transformed_vertices[2].to_vec3();

            // Combine b and c with a and get the resultant vectors.
            const vert_ab: vec.Vec3 = vert_b.vector_sub(&vert_a);
            const vert_ac: vec.Vec3 = vert_c.vector_sub(&vert_a);

            // Get the vector normal by finding the cross product of b-a and c-a
            var normal: vec.Vec3 = vert_ab.vector_cross_product(&vert_ac);
            normal.vector_normalize();

            // Get the camera ray by subtracting vector a by the camera position
            const camera_ray: vec.Vec3 = camera_position.vector_sub(&vert_a);

            // Find out if the normal vector and camera ray are aligned by getting the cross product.
            // It should be noted that we are using a left handed coordinate system ( where positive screen goes
            // into the screen view instead of out ). This affects the way we find the cross and dot product since
            // it's not cummulative. For left handed we do ab x ac and normal . camera_ray. If we had a right handed
            // coordinate system we would reverse the orders here and do ac x ab and camera_ray . normal
            if (normal.vector_dot_product(&camera_ray) <= 0) {
                // The camera is not aligned with this triangle. It can be culled. We'll just skip
                // the rest of the loop by continuing.
                continue;
            }
        }

        // Loop all three vertices and apply projection
        var projected_points: [3]vec.Vec4 = undefined;
        for (0..3) |j| {
            projected_points[j] = Mat4.multiply_and_project(&perspective_matrix, &transformed_vertices[j]);

            projected_points[j].x *= @as(f32, @floatFromInt(display.WIDTH)) / 2.0;
            projected_points[j].y *= @as(f32, @floatFromInt(display.HEIGHT)) / 2.0;
            projected_points[j].x += @as(f32, @floatFromInt(display.WIDTH)) / 2.0;
            projected_points[j].y += @as(f32, @floatFromInt(display.HEIGHT)) / 2.0;
        }

        const avg_depth: f32 = (transformed_vertices[0].z + transformed_vertices[1].z + transformed_vertices[2].z) / 3;
        const projected_triangle: triangle.Triangle = triangle.Triangle.init_all(projected_points[0].to_vec2(), projected_points[1].to_vec2(), projected_points[2].to_vec2(), mesh_face.color, avg_depth);

        triangles_to_render.append(projected_triangle) catch unreachable;
    }
    if (triangles_to_render.items.len >= 1) {
        sort.triangle_quicksort(&triangles_to_render, 0, triangles_to_render.items.len - 1);
    }
}

pub fn render() void {
    display.draw_grid(12, &Color.init(255, 255, 0, 128));
    const size: usize = triangles_to_render.items.len;
    for (0..size) |i| {
        const tri: triangle.Triangle = triangles_to_render.items[i];
        switch (render_mode) {
            .WIRE_AND_VERTEX => {
                display.draw_triangle(tri.points[0].x, tri.points[0].y, tri.points[1].x, tri.points[1].y, tri.points[2].x, tri.points[2].y, &tri.color);
                display.draw_rectangle(@as(isize, @intFromFloat(tri.points[0].x - 2)), @as(isize, @intFromFloat(tri.points[0].y - 2)), 4, 4, &tri.color);
                display.draw_rectangle(@as(isize, @intFromFloat(tri.points[1].x - 2)), @as(isize, @intFromFloat(tri.points[1].y - 2)), 4, 4, &tri.color);
                display.draw_rectangle(@as(isize, @intFromFloat(tri.points[2].x - 2)), @as(isize, @intFromFloat(tri.points[2].y - 2)), 4, 4, &tri.color);
            },
            .WIRE => display.draw_triangle(tri.points[0].x, tri.points[0].y, tri.points[1].x, tri.points[1].y, tri.points[2].x, tri.points[2].y, &tri.color),
            .FILLED => display.draw_filled_triangle(tri.points[0].x, tri.points[0].y, tri.points[1].x, tri.points[1].y, tri.points[2].x, tri.points[2].y, &tri.color),
            .FILLED_AND_WIRE => {
                display.draw_filled_triangle(tri.points[0].x, tri.points[0].y, tri.points[1].x, tri.points[1].y, tri.points[2].x, tri.points[2].y, &tri.color);
                display.draw_triangle(tri.points[0].x, tri.points[0].y, tri.points[1].x, tri.points[1].y, tri.points[2].x, tri.points[2].y, &tri.color);
            },
        }
    }

    triangles_to_render.clearAndFree();
    display.render_color_buffer();
    display.clear_color_buffer(&Color.init(255, 0, 0, 0));
    c.SDL_RenderPresent(display.renderer);
}

pub fn main() !void {
    // const c_alloc = std.heap.c_allocator;
    if (!(display.init_window() and setup())) {
        return;
    }

    while (is_running) {
        process_input();
        update();
        render();
    }

    display.destroy_window();
    std.heap.c_allocator.free(display.color_buffer);
    triangles_to_render.deinit();
    mesh.mesh.faces.deinit();
    mesh.mesh.vertices.deinit();
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
