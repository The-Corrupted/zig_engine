const Color = @import("color.zig").Color;
const Vec2 = @import("vec.zig").Vec2;
const mem = @import("std").mem;
const display = @import("display.zig");

pub const Face = struct {
    a: isize,
    b: isize,
    c: isize,
    color: Color,

    pub fn init(a: isize, b: isize, c: isize, color: Color) Face {
        return .{
            .a = a,
            .b = b,
            .c = c,
            .color = color,
        };
    }
};

pub const Triangle = struct {
    points: [3]Vec2,
    color: Color,
    avg_depth: f32,

    pub fn init(p1: Vec2, p2: Vec2, p3: Vec2) Triangle {
        const points: [3]Vec2 = .{ p1, p2, p3 };
        return .{ .points = points, .color = Color.init(255, 255, 255, 255), .avg_depth = 0.0 };
    }

    pub fn init_all(p1: Vec2, p2: Vec2, p3: Vec2, color: Color, avg_depth: f32) Triangle {
        const points: [3]Vec2 = .{ p1, p2, p3 };
        return .{ .points = points, .color = color, .avg_depth = avg_depth };
    }

    pub fn set_color(self: *Triangle, color: Color) void {
        self.color = color;
    }

    pub fn set_avg_depth(self: *Triangle, avg_depth: f32) void {
        self.avg_depth = avg_depth;
    }
};
