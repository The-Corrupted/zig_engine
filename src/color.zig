pub const Color = struct {
    a: u8,
    r: u8,
    g: u8,
    b: u8,

    pub fn init(a: u8, r: u8, g: u8, b: u8) Color {
        return Color{ .a = a, .r = r, .g = g, .b = b };
    }

    pub fn set_alpha(self: *Color, a: u8) void {
        self.a = a;
    }

    pub fn set_red(self: *Color, r: u8) void {
        self.r = r;
    }

    pub fn set_blue(self: *Color, b: u8) void {
        self.b = b;
    }

    pub fn set_green(self: *Color, g: u8) void {
        self.g = g;
    }

    pub fn green(self: Color) u8 {
        return self.g;
    }

    pub fn blue(self: Color) u8 {
        return self.b;
    }

    pub fn red(self: Color) u8 {
        return self.r;
    }

    pub fn alpha(self: Color) u8 {
        return self.a;
    }

    pub inline fn as_u32(self: Color) u32 {
        var result: u32 = 0x00;
        result |= @as(u32, self.a) << 24;
        result |= @as(u32, self.r) << 16;
        result |= @as(u32, self.g) << 8;
        result |= @as(u32, self.b);
        return result;
    }
};
