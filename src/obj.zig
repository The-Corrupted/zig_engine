const std = @import("std");
const Mesh = @import("mesh.zig").Mesh;
const Face = @import("triangle.zig").Face;
const Vec3 = @import("vec.zig").Vec3;

const MAX_NUM_SIZE: u8 = 15;

pub fn load_face(line: []u8, mesh: *Mesh) !void {
    var face: Face = Face.init(0, 0, 0);
    const length: usize = line.len;

    var count: u8 = 0;
    var index: usize = 1;
    while (index < length) : (index += 1) {
        if (line[index] == ' ') {
            var string: [MAX_NUM_SIZE]u8 = undefined;
            index += 1;

            var string_index: usize = 0;
            while (index < length and line[index] != '/' and line[index] != ' ') : ({
                index += 1;
                string_index += 1;
            }) {
                string[string_index] = line[index];
            }

            const val = try std.fmt.parseInt(isize, string[0..string_index], 10);
            switch (count) {
                0 => face.a = val,
                1 => face.b = val,
                2 => face.c = val,
                else => {},
            }

            index -= 1;
            count += 1;
        }
    }

    try mesh.faces.append(face);
}

pub fn load_vertex(line: []u8, mesh: *Mesh) !void {
    var vertex: Vec3 = Vec3.init(0, 0, 0);
    const length: usize = line.len;

    var count: u8 = 0;
    var index: usize = 1;
    while (index < length) : (index += 1) {
        if (line[index] == ' ') {
            var string: [MAX_NUM_SIZE]u8 = undefined;
            index += 1;

            var string_index: usize = 0;
            while (index < length and line[index] != ' ') : ({
                index += 1;
                string_index += 1;
            }) {
                string[string_index] = line[index];
            }

            const val: f32 = try std.fmt.parseFloat(f32, string[0..string_index]);
            switch (count) {
                0 => vertex.x = val,
                1 => vertex.y = val,
                2 => vertex.z = val,
                else => {},
            }
            index -= 1;
            count += 1;
        }
    }

    try mesh.vertices.append(vertex);
}

pub fn load_obj_file(file_name: []const u8, mesh: *Mesh) bool {
    var file: std.fs.File = undefined;
    if (std.fs.cwd().openFile(file_name, .{})) |handle| {
        file = handle;
    } else |_| {
        std.debug.print("Failed to open file: {s}", .{file_name});
        return false;
    }

    var buf_reader = std.io.bufferedReader(file.reader());
    var stream = buf_reader.reader();

    var buf: [1024]u8 = undefined;

    var reading = true;
    while (reading) {
        if (stream.readUntilDelimiterOrEof(&buf, '\n')) |is_line| {
            if (is_line) |line| {
                if (line[0] == 'v' and line[1] == ' ') {
                    load_vertex(line, mesh) catch unreachable;
                } else if (line[0] == 'f' and line[1] == ' ') {
                    load_face(line, mesh) catch unreachable;
                }
            } else {
                reading = false;
            }
        } else |_| {
            reading = false;
        }
    }

    file.close();
    return true;
}
