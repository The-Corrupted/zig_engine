pub const Vec2 = struct {
    x: f32,
    y: f32,

    pub fn init(x: f32, y: f32) Vec2 {
        return .{
            .x = x,
            .y = y,
        };
    }

    pub inline fn vector_length(self: *Vec2) f32 {
        return @sqrt(self.x * self.x + self.y * self.y);
    }

    pub inline fn vector_add(self: *Vec2, v: *const Vec2) Vec2 {
        return Vec2{ .x = self.x + v.x, .y = self.y + v.y };
    }

    pub inline fn vector_sub(self: *Vec2, v: *const Vec2) Vec2 {
        return Vec2{ .x = self.x - v.x, .y = self.y - v.y };
    }

    pub inline fn vector_scalar_mult(self: *Vec2, factor: f32) Vec2 {
        return Vec2{ .x = self.x * factor, .y = self.y * factor };
    }

    pub inline fn vector_scalar_div(self: *Vec2, factor: f32) Vec2 {
        return Vec2{ .x = self.x / factor, .y = self.y / factor };
    }

    pub inline fn vector_dot_product(self: *Vec2, v: *const Vec2) f32 {
        return (self.x * v.x) + (self.y * v.y);
    }

    pub inline fn vector_normalize(self: *Vec2) void {
        const length: f32 = self.vector_length();
        self.x /= length;
        self.y /= length;
    }
};

pub const Vec3 = struct {
    x: f32,
    y: f32,
    z: f32,

    pub fn init(x: f32, y: f32, z: f32) Vec3 {
        return Vec3{
            .x = x,
            .y = y,
            .z = z,
        };
    }

    pub inline fn vector_length(self: *Vec3) f32 {
        return @sqrt(self.x * self.x + self.y * self.y + self.z * self.z);
    }

    pub inline fn vector_add(self: *const Vec3, v: *const Vec3) Vec3 {
        return Vec3{ .x = self.x + v.x, .y = self.y + v.y, .z = self.z + v.z };
    }

    pub inline fn vector_sub(self: *const Vec3, v: *const Vec3) Vec3 {
        return Vec3{ .x = self.x - v.x, .y = self.y - v.y, .z = self.z - v.z };
    }

    pub inline fn vector_scalar_mult(self: *const Vec3, factor: f32) Vec3 {
        return Vec3{ .x = self.x * factor, .y = self.y * factor, .z = self.z * factor };
    }

    pub inline fn vector_scalar_div(self: *const Vec3, factor: f32) Vec3 {
        return Vec3{ .x = self.x / factor, .y = self.y / factor, .z = self.z / factor };
    }

    pub inline fn vector_cross_product(self: *const Vec3, v: *const Vec3) Vec3 {
        return Vec3{
            .x = (self.y * v.z) - (self.z * v.y),
            .y = (self.z * v.x) - (self.x * v.z),
            .z = (self.x * v.y) - (self.y * v.x),
        };
    }

    pub inline fn vector_dot_product(self: *const Vec3, v: *const Vec3) f32 {
        return (self.x * v.x) + (self.y * v.y) + (self.z * v.z);
    }

    pub fn vector_normalize(self: *Vec3) void {
        const length: f32 = self.vector_length();
        self.x /= length;
        self.y /= length;
        self.z /= length;
    }

    pub fn to_vec2(self: *const Vec3) Vec2 {
        return .{ .x = self.x, .y = self.y };
    }

    pub fn to_vec4(self: *const Vec3) Vec4 {
        return .{ .x = self.x, .y = self.y, .z = self.z, .w = 1.0 };
    }
};

pub fn rotate_x(vec: *Vec3, angle: f32) Vec3 {
    return Vec3.init(
        vec.x,
        vec.y * @cos(angle) - vec.z * @sin(angle),
        vec.y * @sin(angle) + vec.z * @cos(angle),
    );
}

pub fn rotate_y(vec: *Vec3, angle: f32) Vec3 {
    return Vec3.init(
        vec.x * @cos(angle) - vec.z * @sin(angle),
        vec.y,
        vec.x * @sin(angle) + vec.z * @cos(angle),
    );
}

pub fn rotate_z(vec: *Vec3, angle: f32) Vec3 {
    return Vec3.init(
        vec.x * @cos(angle) - vec.y * @sin(angle),
        vec.x * @sin(angle) + vec.y * @cos(angle),
        vec.z,
    );
}

pub const Vec4 = struct {
    x: f32,
    y: f32,
    z: f32,
    w: f32,

    pub fn init(x: f32, y: f32, z: f32, w: f32) Vec4 {
        return .{ .x = x, .y = y, .z = z, .w = w };
    }

    pub inline fn to_vec3(self: *const Vec4) Vec3 {
        return .{ .x = self.x, .y = self.y, .z = self.z };
    }

    pub inline fn to_vec2(self: *const Vec4) Vec2 {
        return .{ .x = self.x, .y = self.y };
    }
};
