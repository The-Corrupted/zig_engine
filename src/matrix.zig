// Rotation matrix consists of the following matrix:
// [ cosA -sinA ]       [x]
//                  *
// [ sinA cosA  ]       [y]
//
// This will give us [ xcosA -ysinA ]       [x1]
//                                      =
//                   [ xsinA + ycosA ]      [y1]
// This produces results that are exactly the same as the manual rotation we performed for vectors: i.e.
// rotate_x = { .x = self.x, .y = self.y * cos(angle) - y * sin(angle), .z = self.z * sin(angle) + self.z * cos(angle)}
// The above is only for rotating two dimensional arrays. A different algorithm is required to rotate a three dimensional array ( 3D matrix )
//
// 4x4 matrices are usually used to represent 3D transformations ( scale, translation, rotation, etc ).
// The 4x4 matrix is requred for very specific transformations ( i.e. translation ) which requires an extra row and column to be performed.
// To enable vector multiplication we add an addition component, w to our vector.
//
// Scale matrix: A matrix who's purpose is to scale the x, y or z values. The scale matrix looks as such.
// [sx 0 0 0]   [x]
// [0 sy 0 0] * [y]
// [0 0 sz 0]   [z]
// [0 0 0  1]   [w]
//
// Translation Matrix:
// The translation matrix is another special matrix that mostly uses an identity matrix to perform vec4 translations.
// The translation matrix looks as such:
// [1, 0, 0, tx]    [x]
// [0, 1, 0, ty] *  [y]
// [0, 0, 1, tz]    [z]
// [0, 0, 0, 1 ]    [1]
//
// Rotation Matrices:
// z rotation:
// [cos(a), -sin(a), 0, 0]  [x]
// [sin(a), cos(a), 0, 0] * [y]
// [0, 0, 1, 0]             [z]
// [0, 0, 0, 1]             [w]
// In the above case, like the vector rotation we did, we're locking the z axis and performing an x and y rotation.
// This will be done basically the same way for rotations and the other axis
// x rotation:
// [1, 0, 0, 0]
// [0 cos(a), -sin(a), 0]
// [0, sin(a), cos(a), 0]
// [0, 0, 0, 1]
//
// y rotation:
// [cos(a), 0, sin(a), 0]
// [0, 1, 0, 0]
// [-sin(a), 0, cos(a), 0]
// [0, 0, 0, 1]
//
// World Matrix: This is the matrix that all transformations are applied to prior to multiplying by the various vertices.
// in order to use the world matrix we want to first create an identity matrix. We then multiply this matrix by all of the transformations we're
// preforming. The order matters and we MUST start with scale, then do rotations and then do translations.
// It needs to be done in this order otherwise we'll get goofy behavior ( matrix multiplication is not cumulative ). In addition to that we MUST
// perform multiplication as such [some_matrix] * [world_matrix]. The matrix on the right-hand side is the matrix that is being multiplied.
// If we do this the otherway around we will, similar to the transformations order, end up with goofy behavior.
//
// Projection Matrix:
// The projection matrix is responsible for a number of tasks
// * Aspect ratio - Adjust the x and y values based on the screen width and height
// * FoV - Adjust the x and y values based on the FoV angle
// * Normalization: Adjust the x, y, z values so that they sit between -1 and 1. This should create an "image space" or normalized device coordinates
// NDC for short that more or less makes up the visible area of your screen ( including imaginary space such as z, creating a cube ).
//
// Aspect ratio - The ratio between the height and width of our monitor resolution ( or the expected/default screen size ).
//
// In order to convert to screen space we need to multiply the x value by the aspect ratio ( h/w )
// [x]                  [a x]
// [y] -> screen space  [y]
// [z]                  [z]
//
// Field of View - The field of view is the angle that captures the view of the objects on screen. We're going to want to get the FoV scaling factor.
// To do that we can break the FoV into two triangles, one making up the left half of FoV and one making up to the right half. We want to get the
// opposite and adjacent lines and in order to do that we do tan(angle/2) which will give us a FoV scaling factor.
//
// The bigger the angle of the FoV the smaller the objects will become because we have to fit more of the objects on the screen. Equally, by reducing the
// FoV angle objects will scale to be larger to fill up screen space. This gives us our final FoV scaling "function"
// f = 1/tan(angle/2) - We use this equation specifically because, by increasing the angle, we actually create a smaller scaling factor and by decreasing
// the FoV angle we get closer to one which increases the scaling factor. This is what we want.
//
// Given all of this our new conversion to screen space looks as such:
// [x]                 [a f x]
// [y] -> screen space [f y]
// [z]                 [z]
//
// We have one last conversion we need to perform now which is normalization. In essence we want to map everything on screen from zfar
// ( the maximum distance we can see ) to znear ( the closest distance we can see before the item is behind us ). In order to do that we'll need to map all
// of the values from 0 to 1 ( where 1 is zfar and 0 is znear ). In order to do this we will take zfar and divide the total distance between zfar and znear
// lmbd = ( zfar/(zfar-znear)) which will give us a scaling factor that will adjust values between the zfar ( 1 ) and znear values. We then need to perform an
// offset on znear:
// (zfar/(zfar-znear)) - (zfar/(zfar-znear) * znear)
//
// Our new conversion to screen space looks like
// [x]    [a * f * x]
// [y] -> [f * y]
// [z]    [lmbd * z - lmbd * znear]
//
// To create our perspective matrix we take the following matrix and use it to multiply our vec4 values.
// [(h/w)(1/tan(angle/2)), 0, 0, 0]
// [0, 1/tan(angle/2), 0, 0]
// [0, 0, zfar/zfar-znear -zfar/zfar-znear * znear]
// [0, 0, 1, 0]

const Vec4 = @import("vec.zig").Vec4;
const std = @import("std");

// We're going to be creating this a lot. There's no sense remaking this over and over. Just keep a local "global" we can copy to a new variable
// when needed
const IDENTITY_MATRIX: Mat4 = Mat4{ .inner = .{ .{ 1, 0, 0, 0 }, .{ 0, 1, 0, 0 }, .{ 0, 0, 1, 0 }, .{ 0, 0, 0, 1 } } };

pub const Mat4 = struct {
    inner: [4][4]f32,

    pub fn init(matrix: [4][4]f32) Mat4 {
        return .{ .inner = matrix };
    }

    pub inline fn create_identity_matrix() Mat4 {
        return IDENTITY_MATRIX;
    }

    // Scale matrix is just an identity matrix that's had a scale factor applied to the x, y, and z positions.
    pub fn create_scale_matrix(x: f32, y: f32, z: f32) Mat4 {
        var mtx = Mat4.create_identity_matrix();
        mtx.inner[0][0] = x;
        mtx.inner[1][1] = y;
        mtx.inner[2][2] = z;
        return mtx;
    }

    pub fn create_perspective_matrix(fov: f32, aspect: f32, znear: f32, zfar: f32) Mat4 {
        const inner: [4][4]f32 = .{.{0.0} ** 4} ** 4;
        var pmtx: Mat4 = Mat4{ .inner = inner };

        std.debug.print("Pmtx pre operation: {any}\n", .{pmtx});

        pmtx.inner[0][0] = aspect * (1 / @tan(fov / 2));
        pmtx.inner[1][1] = 1 / @tan(fov / 2);
        pmtx.inner[2][2] = zfar / (zfar - znear);
        pmtx.inner[2][3] = (-zfar * znear) / (zfar - znear);
        pmtx.inner[3][2] = 1.0;

        std.debug.print("Pmtx post operation: {any}\n", .{pmtx});
        return pmtx;
    }

    pub fn create_translation_matrix(x: f32, y: f32, z: f32) Mat4 {
        var mtx = Mat4.create_identity_matrix();
        mtx.inner[0][3] = x;
        mtx.inner[1][3] = y;
        mtx.inner[2][3] = z;
        return mtx;
    }

    pub fn create_rotation_x(angle: f32) Mat4 {
        var mtx = Mat4.create_identity_matrix();
        const c = @cos(angle);
        const s = @sin(angle);

        mtx.inner[1][1] = c;
        mtx.inner[1][2] = -s;
        mtx.inner[2][1] = s;
        mtx.inner[2][2] = c;

        return mtx;
    }

    pub fn create_rotation_y(angle: f32) Mat4 {
        var mtx = Mat4.create_identity_matrix();
        const c = @cos(angle);
        const s = @sin(angle);

        mtx.inner[0][0] = c;
        mtx.inner[0][2] = s;
        mtx.inner[2][0] = -s;
        mtx.inner[2][2] = c;

        return mtx;
    }

    pub fn create_rotation_z(angle: f32) Mat4 {
        var mtx = Mat4.create_identity_matrix();
        const c = @cos(angle);
        const s = @sin(angle);

        mtx.inner[0][0] = c;
        mtx.inner[0][1] = -s;
        mtx.inner[1][0] = s;
        mtx.inner[1][1] = c;

        return mtx;
    }

    // This takes a scale matrix ( an identity matrix where the values have been modified to include the scaling we want to do on a vec4)
    // and multiply a vec4 by the matrix. This will scale all everything in the vec4 by whatever the scale factor was that was set on
    // this matrix. This is an instance method that does not change the instance itself or the vector passed to it. It will return
    // a new vector that's been scaled.
    pub fn multiply_vector4(self: *const Mat4, vec: *const Vec4) Vec4 {
        var result = Vec4.init(0, 0, 0, 1);
        result.x = self.inner[0][0] * vec.x + self.inner[0][1] * vec.y + self.inner[0][2] * vec.z + self.inner[0][3] * vec.w;
        result.y = self.inner[1][0] * vec.x + self.inner[1][1] * vec.y + self.inner[1][2] * vec.z + self.inner[1][3] * vec.w;
        result.z = self.inner[2][0] * vec.x + self.inner[2][1] * vec.y + self.inner[2][2] * vec.z + self.inner[2][3] * vec.w;
        result.w = self.inner[3][0] * vec.x + self.inner[3][1] * vec.y + self.inner[3][2] * vec.z + self.inner[3][3] * vec.w;
        return result;
    }

    pub fn multiply_and_project(pmtx: *const Mat4, vec: *const Vec4) Vec4 {
        var result = Mat4.multiply_vector4(pmtx, vec);

        if (result.w != 0.0) {
            result.x /= result.w;
            result.y /= result.w;
            result.z /= result.w;
        }

        return result;
    }

    pub fn multiply_matrix4(mat1: *const Mat4, mat2: *const Mat4) Mat4 {
        var m: Mat4 = undefined;
        for (0..4) |row| {
            for (0..4) |col| {
                m.inner[row][col] = mat1.inner[row][0] * mat2.inner[0][col] + mat1.inner[row][1] * mat2.inner[1][col] + mat1.inner[row][2] * mat2.inner[2][col] + mat1.inner[row][3] * mat2.inner[3][col];
            }
        }
        return m;
    }

    pub fn leak_inner(self: *Mat4) [4][4]f32 {
        return self.inner;
    }
};
