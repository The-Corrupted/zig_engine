const std = @import("std");
const Vec3 = @import("vec.zig").Vec3;
const Face = @import("triangle.zig").Face;
const Color = @import("color.zig").Color;

pub const Mesh = struct {
    vertices: std.ArrayListAligned(Vec3, null),
    faces: std.ArrayListAligned(Face, null),
    rotation: Vec3,
    scale: Vec3,
    translation: Vec3,
};

const N_CUBE_VERTICES: u32 = 8;
const N_CUBE_FACES: u32 = (6 * 2);

pub var mesh: Mesh = .{
    .vertices = std.ArrayList(Vec3).init(std.heap.c_allocator),
    .faces = std.ArrayList(Face).init(std.heap.c_allocator),
    .rotation = Vec3.init(0, 0, 0),
    .scale = Vec3.init(1.0, 1.0, 1.0),
    .translation = Vec3.init(0.0, 0.0, 0.0),
};

const cube_vertices: [N_CUBE_VERTICES]Vec3 = .{
    Vec3.init(-1, -1, -1), Vec3.init(-1, 1, -1), Vec3.init(1, 1, -1),
    Vec3.init(1, -1, -1),  Vec3.init(1, 1, 1),   Vec3.init(1, -1, 1),
    Vec3.init(-1, 1, 1),   Vec3.init(-1, -1, 1),
};

const cube_faces: [N_CUBE_FACES]Face = .{
    Face.init(1, 2, 3, Color.init(255, 255, 0, 0)),   Face.init(1, 3, 4, Color.init(255, 255, 0, 0)),   Face.init(4, 3, 5, Color.init(255, 0, 255, 0)),
    Face.init(4, 5, 6, Color.init(255, 0, 255, 0)),   Face.init(6, 5, 7, Color.init(255, 0, 0, 255)),   Face.init(6, 7, 8, Color.init(255, 0, 0, 255)),
    Face.init(8, 7, 2, Color.init(255, 255, 255, 0)), Face.init(8, 2, 1, Color.init(255, 255, 255, 0)), Face.init(2, 7, 5, Color.init(255, 255, 0, 255)),
    Face.init(2, 5, 3, Color.init(255, 255, 0, 255)), Face.init(6, 8, 1, Color.init(255, 0, 255, 255)), Face.init(6, 1, 4, Color.init(255, 0, 255, 255)),
};

pub fn load_cube_mesh_data() void {
    for (0..N_CUBE_VERTICES) |i| {
        const cube_vertex: Vec3 = cube_vertices[i];
        mesh.vertices.append(cube_vertex) catch unreachable;
    }

    for (0..N_CUBE_FACES) |i| {
        const cube_face: Face = cube_faces[i];
        mesh.faces.append(cube_face) catch unreachable;
    }
}
