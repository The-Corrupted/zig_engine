const std = @import("std");
const c = @import("c.zig");
const Color = @import("color.zig").Color;
const Triangle = @import("triangle.zig").Triangle;

pub const FPS: u32 = 60;
pub const FRAME_TARGET_TIME: u32 = (1000 / FPS);
pub var window: ?*c.SDL_Window = null;
pub var renderer: ?*c.SDL_Renderer = null;
pub var color_buffer_texture: ?*c.SDL_Texture = null;
pub var WIDTH: u32 = 1920;
pub var HEIGHT: u32 = 1080;
pub var color_buffer: []u32 = undefined;

pub const RenderMode = enum {
    WIRE_AND_VERTEX,
    WIRE,
    FILLED,
    FILLED_AND_WIRE,
};

pub fn init_window() bool {
    if (c.SDL_Init(c.SDL_INIT_EVERYTHING) != 0) {
        std.debug.print("ERROR: Failed to initialize SDL\n", .{});
        return false;
    }

    var display_mode: c.SDL_DisplayMode = undefined;
    if (c.SDL_GetCurrentDisplayMode(0, &display_mode) < 0) {
        std.debug.print("ERROR: Failed to get display mode", .{});
        return false;
    }

    WIDTH = @intCast(@divFloor(display_mode.w, 2));
    HEIGHT = @intCast(@divFloor(display_mode.h, 2));

    const window_name = "3D Graphics From Scratch";
    window = c.SDL_CreateWindow(window_name, c.SDL_WINDOWPOS_CENTERED, c.SDL_WINDOWPOS_CENTERED, @intCast(WIDTH), @intCast(HEIGHT), c.SDL_WINDOW_VULKAN);

    if (window == null) {
        std.debug.print("ERROR: Failed to create window\n", .{});
        return false;
    }

    renderer = c.SDL_CreateRenderer(window, -1, 0);

    if (renderer == null) {
        std.debug.print("Failed to create renderer\n", .{});
        return false;
    }

    return true;
}

pub fn render_color_buffer() void {
    const size: c_int = @intCast(WIDTH * @sizeOf(u32));
    _ = c.SDL_UpdateTexture(color_buffer_texture, null, @as(*anyopaque, color_buffer.ptr), size);
    _ = c.SDL_RenderCopy(renderer, color_buffer_texture, null, null);
}

pub fn clear_color_buffer(color: *const Color) void {
    const color_u32 = color.as_u32();
    for (0..HEIGHT) |y| {
        for (0..WIDTH) |x| {
            const pixel = (WIDTH * y) + x;
            color_buffer[pixel] = color_u32;
        }
    }
}

pub fn draw_grid(size: u32, color: *const Color) void {
    const color_u32 = color.as_u32();
    var y: u32 = 0;
    while (y < HEIGHT) : (y += size) {
        var x: u32 = 0;
        while (x < WIDTH) : (x += size) {
            const pixel: usize = @as(usize, (WIDTH * y) + x);
            color_buffer[pixel] = color_u32;
        }
    }
}

pub inline fn draw_pixel(x: isize, y: isize, color: *const Color) void {
    const color_u32 = color.as_u32();
    if (x >= 0 and x < WIDTH and y >= 0 and y < HEIGHT) {
        const pixel: usize = @intCast((WIDTH * y) + x);
        color_buffer[pixel] = color_u32;
    }
}

pub fn draw_rectangle(x: isize, y: isize, width: isize, height: isize, color: *const Color) void {
    const x_end: isize = x + width;
    const y_end: isize = y + height;
    var y1: isize = y;
    while (y1 <= y_end) : (y1 += 1) {
        var x1: isize = x;
        while (x1 <= x_end) : (x1 += 1) {
            draw_pixel(x1, y1, color);
        }
    }
}

pub fn plot_line_low_bresen(x0: isize, y0: isize, x1: isize, y1: isize, color: *const Color) void {
    const dx: isize = x1 - x0;
    var dy: isize = y1 - y0;

    var yi: isize = 1;

    if (dy < 0) {
        yi = -1;
        dy = -dy;
    }

    var D: isize = (2 * dy) - dx;
    var y: isize = y0;
    var x: isize = x0;
    while (x < x1) : (x += 1) {
        draw_pixel(x, y, color);
        if (D > 0) {
            y = y + yi;
            D = D + (2 * (dy - dx));
        } else {
            D = D + 2 * dy;
        }
    }
}

pub fn plot_line_high_bresen(x0: isize, y0: isize, x1: isize, y1: isize, color: *const Color) void {
    var dx: isize = x1 - x0;
    const dy: isize = y1 - y0;

    var xi: isize = 1;
    if (dx < 0) {
        xi = -1;
        dx = -dx;
    }

    var D: isize = (2 * dx) - dy;
    var x: isize = x0;
    var y: isize = y0;
    while (y < y1) : (y += 1) {
        draw_pixel(x, y, color);
        if (D > 0) {
            x = x + xi;
            D = D + (2 * (dx - dy));
        } else {
            D = D + 2 * dx;
        }
    }
}

pub fn draw_line_bresen(x0: isize, y0: isize, x1: isize, y1: isize, color: *const Color) void {
    if (@abs(y1 - y0) < @abs(x1 - x0)) {
        if (x0 > x1) {
            plot_line_low_bresen(x1, y1, x0, y0, color);
        } else {
            plot_line_low_bresen(x0, y0, x1, y1, color);
        }
    } else {
        if (y0 > y1) {
            plot_line_high_bresen(x1, y1, x0, y0, color);
        } else {
            plot_line_high_bresen(x0, y0, x1, y1, color);
        }
    }
}

pub fn draw_triangle(t_x0: f32, t_y0: f32, t_x1: f32, t_y1: f32, t_x2: f32, t_y2: f32, color: *const Color) void {
    // Convert points to isize for draw_line_bresen. We do this here so we don't duplicate the conversion code later
    const x0: isize = @as(isize, @intFromFloat(t_x0));
    const y0: isize = @as(isize, @intFromFloat(t_y0));
    const x1: isize = @as(isize, @intFromFloat(t_x1));
    const y1: isize = @as(isize, @intFromFloat(t_y1));
    const x2: isize = @as(isize, @intFromFloat(t_x2));
    const y2: isize = @as(isize, @intFromFloat(t_y2));

    draw_line_bresen(x0, y0, x1, y1, color);
    draw_line_bresen(x1, y1, x2, y2, color);
    draw_line_bresen(x2, y2, x0, y0, color);
}

pub fn draw_filled_triangle(t_x0: f32, t_y0: f32, t_x1: f32, t_y1: f32, t_x2: f32, t_y2: f32, color: *const Color) void {
    // Sort vertices by y coordinate
    var x0: isize = @as(isize, @intFromFloat(t_x0));
    var y0: isize = @as(isize, @intFromFloat(t_y0));
    var x1: isize = @as(isize, @intFromFloat(t_x1));
    var y1: isize = @as(isize, @intFromFloat(t_y1));
    var x2: isize = @as(isize, @intFromFloat(t_x2));
    var y2: isize = @as(isize, @intFromFloat(t_y2));

    if (y0 > y1) {
        std.mem.swap(isize, &y0, &y1);
        std.mem.swap(isize, &x0, &x1);
    }

    if (y1 > y2) {
        std.mem.swap(isize, &y1, &y2);
        std.mem.swap(isize, &x1, &x2);
    }

    if (y0 > y1) {
        std.mem.swap(isize, &y0, &y1);
        std.mem.swap(isize, &x0, &x1);
    }

    if (y1 == y2) {
        fill_flat_bottom_triangle(x0, y0, x1, y1, x2, y2, color);
    } else if (y0 == y1) {
        fill_flat_top_triangle(x0, y0, x1, y1, x2, y2, color);
    } else {

        // Calculate MX, MY vertex
        const my: isize = y1;
        const mx: isize = @as(isize, @intFromFloat(@as(f32, @floatFromInt((x2 - x0) * (y1 - y0))) / @as(f32, @floatFromInt((y2 - y0))) + @as(f32, @floatFromInt(x0))));

        // Draw flat bottom triangle
        fill_flat_bottom_triangle(x0, y0, x1, y1, mx, my, color);

        // Draw flat top triangle
        fill_flat_top_triangle(x1, y1, mx, my, x2, y2, color);
    }
}

fn fill_flat_bottom_triangle(x0: isize, y0: isize, x1: isize, y1: isize, x2: isize, y2: isize, color: *const Color) void {
    // Calculate the slope of the two sides of the flat bottom.
    // Calculate the x start and x end for each line between y0 and my.
    // Draw pixels between x start and x end. Do this in a loop until we reach my

    const inverse_slope1: f64 = @as(f64, @floatFromInt((x1 - x0))) / @as(f64, @floatFromInt((y1 - y0)));
    const inverse_slope2: f64 = @as(f64, @floatFromInt((x2 - x0))) / @as(f64, @floatFromInt((y2 - y0)));

    // Loop all the scan lines from top to bottoom
    var y: isize = y0;
    // start_x and x_end from top.
    var x_start: f64 = @as(f64, @floatFromInt(x0));
    var x_end: f64 = x_start;
    while (y <= y2) : ({
        y += 1;
        x_start += inverse_slope1;
        x_end += inverse_slope2;
    }) {
        draw_line_bresen(@as(isize, @intFromFloat(x_start)), y, @as(isize, @intFromFloat(x_end)), y, color);
    }
}

fn fill_flat_top_triangle(x0: isize, y0: isize, x1: isize, y1: isize, x2: isize, y2: isize, color: *const Color) void {
    // Calculate the slope of the two sides of the flat bottom.
    // Calculate the x start and x end for each line between y0 and my.
    // Draw pixels between x start and x end. Do this in a loop until we reach my

    const inverse_slope1: f64 = @as(f64, @floatFromInt((x2 - x0))) / @as(f64, @floatFromInt((y2 - y0)));
    const inverse_slope2: f64 = @as(f64, @floatFromInt((x2 - x1))) / @as(f64, @floatFromInt((y2 - y1)));
    // Loop all the scan lines from bottom to top
    var y: isize = y2;
    // start_x and x_end from top.
    var x_start: f64 = @as(f64, @floatFromInt(x2));
    var x_end: f64 = x_start;
    while (y >= y0) : ({
        y -= 1;
        x_start -= inverse_slope1;
        x_end -= inverse_slope2;
    }) {
        draw_line_bresen(@as(isize, @intFromFloat(x_start)), y, @as(isize, @intFromFloat(x_end)), y, color);
    }
}

pub fn destroy_window() void {
    std.debug.print("Destroying window\n", .{});
    c.SDL_DestroyRenderer(renderer);
    c.SDL_DestroyWindow(window);
    c.SDL_Quit();
}
