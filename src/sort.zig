const triangle = @import("triangle.zig");
const std = @import("std");

pub fn triangle_insert_sort(triangles: *std.ArrayListAligned(triangle.Triangle, null)) void {
    var current_index: usize = 0;
    const end = triangles.items.len;
    while (current_index < end) : (current_index += 1) {
        for (0..end) |i| {
            if (triangles.items[current_index].avg_depth > triangles.items[i].avg_depth) {
                std.mem.swap(triangle.Triangle, &triangles.items[current_index], &triangles.items[i]);
            }
        }
    }
}

fn partition(triangles: *std.ArrayListAligned(triangle.Triangle, null), low: usize, high: usize) usize {
    const pivot: f32 = triangles.items[high].avg_depth;
    var i: usize = low;

    for (low..high) |j| {
        if (triangles.items[j].avg_depth >= pivot) {
            std.mem.swap(triangle.Triangle, &triangles.items[i], &triangles.items[j]);
            i += 1;
        }
    }

    // Swap the pivot element into its correct position
    std.mem.swap(triangle.Triangle, &triangles.items[i], &triangles.items[high]);

    return i;
}

pub fn triangle_quicksort(triangles: *std.ArrayListAligned(triangle.Triangle, null), low: usize, high: usize) void {
    if (low < high) {
        const pivot_index = partition(triangles, low, high);

        // Ensure pivot_index - 1 doesn't underflow
        if (pivot_index > 0) {
            triangle_quicksort(triangles, low, pivot_index - 1);
        }

        triangle_quicksort(triangles, pivot_index + 1, high);
    }
}
