A zig port of the 3dengine project/course

Build:
zig build --release=small

Run:
./zig-out/bin/zig_engine
